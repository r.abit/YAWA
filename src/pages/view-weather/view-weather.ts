import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ViewWeatherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-weather',
  templateUrl: 'view-weather.html',
})
export class ViewWeatherPage {

  city;
  country;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.city = navParams.data.city.name;
    this.country = navParams.data.city.country;
    console.log(this.city + " Yessss...");
    console.log(navParams.data.list);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ViewWeatherPage');
  }

}
