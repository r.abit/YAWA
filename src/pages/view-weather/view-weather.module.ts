import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewWeatherPage } from './view-weather';

@NgModule({
  declarations: [
    ViewWeatherPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewWeatherPage),
  ],
})
export class ViewWeatherPageModule {}
