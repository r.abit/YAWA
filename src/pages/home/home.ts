import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { WeatherDataProvider } from '../../providers/weather-data/weather-data';
import { ViewWeatherPage} from "../view-weather/view-weather";

import { AlertController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

  vienna: any = [];
  ennenda: any = [];
  samoljica: any = [];

  city: any = [];
  viewWeather = ViewWeatherPage;

  constructor(public navCtrl: NavController,
              public weatherService: WeatherDataProvider,
              public alertCtrl: AlertController) {

    this.getVienna();
    this.getEnnenda();
    this.getSamoljica();
  }

  getVienna() {
    this.weatherService.getViennaData()
    .then(data => {
      this.vienna = data;
      this.city = this.vienna.list;
      console.log(this.vienna);
    });
  }

  getEnnenda() {
    this.weatherService.getEnnendaData()
    .then(data => {
      this.ennenda = data;
      this.city = this.ennenda.list;
      console.log(this.ennenda);
    });
  }

  getSamoljica() {
    this.weatherService.getSamoljicaData()
    .then(data => {
      this.samoljica = data;
      this.city = this.samoljica.list;
      console.log(this.samoljica);
    });
  }

  //for the future plans to build dynamic
  // getCityId() {
  //   this.weatherService.getCity()
  //   .then(data => {
  //     this.cityId = data;
  //     console.log(this.cityId);
  //   });
  // }

 navigateToOtherPage(item: any) {
   console.log(item);
  this.navCtrl.push(ViewWeatherPage, item);
}

}