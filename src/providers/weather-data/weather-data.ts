import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';


/*
  Generated class for the WeatherDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class WeatherDataProvider {

  constructor(public http: HttpClient) {
    console.log('Hello WeatherDataProvider Provider');
  }

  // ennenda -> cityID = '7285732';
  ennendaUrl = 'http://api.openweathermap.org/data/2.5/forecast?id=7285732&APPID=bb9aa393275f6e2d7e99cd50d3304da1'

  // samoljica -> cityID = '786111';
  samoljicaUrl = 'http://api.openweathermap.org/data/2.5/forecast?id=786111&APPID=bb9aa393275f6e2d7e99cd50d3304da1';

  // wien
  cityID = '2761369';

  apiUrl = 'http://api.openweathermap.org/data/2.5/forecast?id='+
    this.cityID +
    '&APPID=bb9aa393275f6e2d7e99cd50d3304da1';

  cityIdUrl = '../../../assets/city.list.json';
  
  getViennaData(){
    return new Promise((resolve, reject) => {
      this.http.get(this.apiUrl)
        .subscribe(res => { resolve(res); },
          (err) => { reject(err); }
        );
    });
  }

  getEnnendaData(){
    return new Promise((resolve, reject) => {
      this.http.get(this.ennendaUrl)
        .subscribe(res => { resolve(res); },
          (err) => { reject(err); }
        );
    });
  }

  getSamoljicaData(){
    return new Promise((resolve, reject) => {
      this.http.get(this.samoljicaUrl)
        .subscribe(res => { resolve(res); },
          (err) => { reject(err); }
        );
    });
  }

  // getCity(){
  //   return new Promise((resolve, reject) => {
  //     this.http.get(this.cityIdUrl)
  //       .subscribe(res => { resolve(res); },
  //         (err) => { reject(err); }
  //       );
  //   });
  // }
}